PREFIX?=/usr

install:
	cp -p src/once.sh ${DESTDIR}${PREFIX}/bin/once
	cp -p src/completion/once.bash-completion ${DESTDIR}${PREFIX}/share/bash-completion/completions/once
